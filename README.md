# keyboard

xkb keyboard layout for developers that require German umlauts.

## Core aspects

* Uses Capslock as Ctrl-Alt
* Supports German Umlauts and Czech letters (and probably some other languages too)
* Embraces Linux Console Command shortcuts. Examples:
  * Capslock + j simulates Return
  * Capslock + h deletes a character

## Key setup

Please refer to the comments in [rbkb](rbkb). They should tell you everything you need to know.

### Installing

Download [rbkb](rbkb) to your xkb/symbols folder:
```
sudo wget -P /usr/share/X11/xkb/symbols https://gitlab.com/rblau/keyboard/raw/master/rbkb

```
Make sure it is downloaded under the correct file name - especially if you're updating the layout.

Enable it:
```
setxkbmap rbkb
```

To make the change permanent, add `setxkbmap rbkb` to a script that launches with your X-session.


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
